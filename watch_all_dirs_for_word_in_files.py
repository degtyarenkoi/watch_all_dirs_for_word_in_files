import os, re, time
from datetime import datetime
import calendar


def get_names():
    """
    get_names() method searches a specified word through directories
    :return: hostname of the entire file name
    """
    for top, dirs, files in os.walk(os.getcwd() + "/grants"):
        for name in files:
            f = open(os.path.join(top, name))
            file_content = f.read()
            if "joep" in file_content:
                file_creation_time = int(datetime.fromtimestamp(os.path.getmtime(os.path.join(top, name))).strftime("%j"))

                today = int(datetime.now().strftime("%j"))
                if today > 1:
                    yesterday = today -1
                else:
                    yesterday = 366 if calendar.isleap(int(str(datetime.now()[:4])-1)) else 365

                if yesterday == file_creation_time:
                    matched_file = re.findall("[a-z-]+[a-z-]+[0-9]+\.\w+\.[a-z]+\.[a-z]+", name)[0]
                    print matched_file


if __name__ == "__main__":
    get_names()

